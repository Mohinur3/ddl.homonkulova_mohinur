CREATE DATABASE mountain_climbing_db;
CREATE SCHEMA mountain_climbing_schema;

CREATE TABLE Country (
    CountryID SERIAL PRIMARY KEY,
    CountryName VARCHAR(255) NOT NULL
);
CREATE TABLE Area (
    AreaID SERIAL PRIMARY KEY,
    AreaName VARCHAR(255) NOT NULL,
    CountryID INT NOT NULL,
    CONSTRAINT fk_country FOREIGN KEY (CountryID) REFERENCES Country(CountryID)
);
CREATE TABLE Mountain (
    MountainID SERIAL PRIMARY KEY,
    MountainName VARCHAR(255) NOT NULL,
    Height INT NOT NULL,
    CountryID INT NOT NULL,
    AreaID INT NOT NULL,
    CONSTRAINT fk_country FOREIGN KEY (CountryID) REFERENCES Country(CountryID),
    CONSTRAINT fk_area FOREIGN KEY (AreaID) REFERENCES Area(AreaID)
);
CREATE TABLE Climber (
    ClimberID SERIAL PRIMARY KEY,
    FirstName VARCHAR(255) NOT NULL,
    LastName VARCHAR(255) NOT NULL,
    Address VARCHAR(255),
    Email VARCHAR(255) NOT NULL,
    Phone VARCHAR(20),
    Gender VARCHAR(10) NOT NULL CHECK (Gender IN ('Male', 'Female', 'Other'))
);
CREATE TABLE Climb (
    ClimbID SERIAL PRIMARY KEY,
    ClimberID INT NOT NULL,
    MountainID INT NOT NULL,
    StartDate DATE NOT NULL CHECK (StartDate > '2000-01-01'),
    EndDate DATE NOT NULL,
    CONSTRAINT fk_climber FOREIGN KEY (ClimberID) REFERENCES Climber(ClimberID),
    CONSTRAINT fk_mountain FOREIGN KEY (MountainID) REFERENCES Mountain(MountainID)
);
CREATE TABLE ClimbParticipants (
    ClimbParticipantID SERIAL PRIMARY KEY,
    ClimbID INT NOT NULL,
    ClimberID INT NOT NULL,
    Role VARCHAR(255) NOT NULL,
    CONSTRAINT fk_climb FOREIGN KEY (ClimbID) REFERENCES Climb(ClimbID),
    CONSTRAINT fk_climber FOREIGN KEY (ClimberID) REFERENCES Climber(ClimberID)
);
CREATE TABLE ClimberClimb (
    ClimberClimbID SERIAL PRIMARY KEY,
    ClimberID INT NOT NULL,
    ClimbID INT NOT NULL,
    CONSTRAINT fk_climber FOREIGN KEY (ClimberID) REFERENCES Climber(ClimberID),
    CONSTRAINT fk_climb FOREIGN KEY (ClimbID) REFERENCES Climb(ClimbID)
);
CREATE TABLE Weather (
    WeatherID SERIAL PRIMARY KEY,
    ClimbID INT NOT NULL,
    Temperature DECIMAL(5,2),
    Conditions VARCHAR(255),
    WindSpeed DECIMAL(5,2),
    CONSTRAINT fk_climb FOREIGN KEY (ClimbID) REFERENCES Climb(ClimbID)
);
CREATE TABLE Equipment (
    EquipmentID SERIAL PRIMARY KEY,
    EquipmentName VARCHAR(255) NOT NULL
);
CREATE TABLE ClimbEquipment (
    ClimbEquipmentID SERIAL PRIMARY KEY,
    ClimbID INT NOT NULL,
    EquipmentID INT NOT NULL,
    Quantity INT NOT NULL CHECK (Quantity >= 0),
    CONSTRAINT fk_climb FOREIGN KEY (ClimbID) REFERENCES Climb(ClimbID),
    CONSTRAINT fk_equipment FOREIGN KEY (EquipmentID) REFERENCES Equipment(EquipmentID)
);
CREATE TABLE Accidents (
    AccidentID SERIAL PRIMARY KEY,
    ClimbID INT NOT NULL,
    Description TEXT,
    InjuryType VARCHAR(255),
    CONSTRAINT fk_climb FOREIGN KEY (ClimbID) REFERENCES Climb(ClimbID)
);

ALTER TABLE Country ADD COLUMN record_ts DATE DEFAULT current_date;
ALTER TABLE Area ADD COLUMN record_ts DATE DEFAULT current_date;
ALTER TABLE Mountain ADD COLUMN record_ts DATE DEFAULT current_date;
ALTER TABLE Climber ADD COLUMN record_ts DATE DEFAULT current_date;
ALTER TABLE Climb ADD COLUMN record_ts DATE DEFAULT current_date;
ALTER TABLE ClimbParticipants ADD COLUMN record_ts DATE DEFAULT current_date;
ALTER TABLE ClimberClimb ADD COLUMN record_ts DATE DEFAULT current_date;
ALTER TABLE Weather ADD COLUMN record_ts DATE DEFAULT current_date;
ALTER TABLE Equipment ADD COLUMN record_ts DATE DEFAULT current_date;
ALTER TABLE ClimbEquipment ADD COLUMN record_ts DATE DEFAULT current_date;
ALTER TABLE Accidents ADD COLUMN record_ts DATE DEFAULT current_date;

INSERT INTO Country (CountryName) VALUES
    ('USA'),
    ('Canada'),
    ('France'),
    ('Nepal');
INSERT INTO Area (AreaName, CountryID) VALUES
    ('Rockies', 1),
    ('Canadian Rockies', 2),
    ('Alps', 3),
    ('Himalayas', 4);
INSERT INTO Mountain (MountainName, Height, CountryID, AreaID) VALUES
    ('Mount Everest', 8848, 4, 4),
    ('Denali', 6190, 1, 1),
    ('Mont Blanc', 4810, 3, 3);   
INSERT INTO Climber (FirstName, LastName, Email, Gender) VALUES
    ('John', 'Doe', 'john.doe@example.com', 'Male'),
    ('Jane', 'Smith', 'jane.smith@example.com', 'Female');   
INSERT INTO Climb (ClimberID, MountainID, StartDate, EndDate) VALUES
    (1, 1, '2023-05-15', '2023-06-01'),
    (2, 3, '2023-07-10', '2023-07-25');
INSERT INTO ClimbParticipants (ClimbID, ClimberID, Role) VALUES
    (1, 1, 'Leader'),
    (1, 2, 'Participant'),
    (2, 2, 'Leader'); 
INSERT INTO ClimberClimb (ClimberID, ClimbID) VALUES
    (1, 1),
    (2, 1),
    (2, 2);  
INSERT INTO Weather (ClimbID, Temperature, Conditions, WindSpeed) VALUES
    (1, -10.5, 'Snowy', 15.5),
    (2, -5.0, 'Sunny', 5.0); 
INSERT INTO Equipment (EquipmentName) VALUES
    ('Rope'),
    ('Ice Axe'),
    ('Crampons'); 
INSERT INTO ClimbEquipment (ClimbID, EquipmentID, Quantity) VALUES
    (1, 1, 2),
    (1, 2, 1),
    (2, 1, 1);
INSERT INTO Accidents (ClimbID, Description, InjuryType) VALUES
    (1, 'Slip and fall on ice', 'Fracture'),
    (2, 'Minor frostbite', 'Frostbite');   
    
SELECT * FROM Country;
SELECT * FROM Area;
SELECT * FROM Mountain;
SELECT * FROM Climber;
SELECT * FROM Climb;
SELECT * FROM ClimbParticipants;
SELECT * FROM ClimberClimb;
SELECT * FROM Weather;
SELECT * FROM Equipment;
SELECT * FROM ClimbEquipment;
SELECT * FROM Accidents;   